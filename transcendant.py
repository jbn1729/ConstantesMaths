from constantes import *

class Expmx_mx(Constante):# omega
	def __init__(self, nbd):
		super().__init__(nbd)
		self.name = 'exp(-x)-x'
	
	def _1(self):#la seul que je connaisse, utilise la méthode de newton
		un = self.un
		xav, x = 0, un >> 1
		n = 1
		while (1):
			x, xav = (x+un)*un//(self.expo(x, un)+un), x
			t = x-xav
			if abs(t) < 11:return x+xav >> 1
			print('stape', n, 'erreur ~= 10 ^', len(str(t)))
			n += 1
		return x

class E(Constante):#https://en.wikipedia.org/wiki/Euler%27s_number
	def __init__(self, nbd):
		self.nbd = nbd
		self.un = mpz(1)<<nbd
		self.name = 'e'
	
	def _1(self):
		"""exp(1)"""
		nbd, un = self.nbd, self.un
		i = s = un << 1
		pui = un*un//i
		sav = 0
		while (s != sav):
			sav = s
			s += pui
			i += un
			pui = (pui<<nbd)//i
		return s
		
class Pi(Constante):#https://en.wikipedia.org/wiki/Pi_(constant)
	def __init__(self, nbd):
		super().__init__(nbd)
		self.name = 'Pi'
		self.link += 'Pi_(constant)'
	
	@cache	
	def _1(self):
		"""formule de Chudnovsky"""
		nbd = self.nbd
		cube = mpz(-320160)**3//3
		un = self.un
		fac = self.un           #	    (6i)!
								#_________________
								#(3i)!(i!)³×cube*8
		addit = mpz(13591409)#13591409+545140134i
		_545140134 = mpz(545140134)
		i = mpz(0)
		_1 = mpz(1)
		s = mpz(0)
		sav = mpz(1)
		while s != sav:
			sav = s
			s += fac*addit
			i += _1
			fac = fac*(6*i-1)*(2*i-1)*(6*i-5)//(cube*i*i*i)
			addit += _545140134
		return isqrt(10005*un**4)*426880//s, i
		
class Ln2(Constante):
	def __init__(self, nbd):
		super().__init__(nbd)
		self.name = 'ln 2'
		self.link += ''
	
	@cache
	def _1(self):
		un = self.un
		nbd = self.nbd
		a = 14*un//31
		b = 6*un//161
		c = 10*un//49
		s0 = s1 = s2 = 0
		sav0 = sav1 = sav2 = 1
		pui961 = mpz(1)
		k = 1
		while s0 != sav0:
			sav0 = s0
			s0 += a//(pui961*k)
			pui961 *= 961
			k += 2
		pui25921 = mpz(1)
		k = 1
		while s2 != sav2:
			sav2 = s2
			s2 += b//(pui25921*k)
			pui25921 *= 25921
			k += 2
		pui2401 = mpz(1)
		k = 1
		while s1 != sav1:
			sav1 = s1
			s1 += c//(pui2401*k)
			pui2401 *= 2401
			k += 2
		return s0+s1+s2
		
class Universe(Constante):
	def __init__(self, nbd):
		super().__init__(nbd)
		self.name = 'Universal parabolic'
		self.link += ''	
	
	@cache
	def _1(self):
		return self.iln(un+isqrt(2*un*un))+isqrt(2*un*un)

class Ramanujan(Constante):
	def __init__(self, nbd):
		super().__init__(nbd)
		self.name = 'Ramanujan'
	
	@cache
	def _1(self):
		pi_ = Pi(self.nbd)._1()[0]
		return self.expo(pi_*isqrt(163*self.un**2)//self.un, self.un)
		
class exppi(Constante):
	def __init__(self, nbd):
		super().__init__(nbd)
		self.name = 'exp(pi)'
	
	@cache
	def _1(self):
		return self.expo(Pi(self.nbd)._1()[0], self.un)
		
class Lemniscate(Constante):#https://en.wikipedia.org/wiki/Lemniscate_constant
	def __init__(self, nbd):
		super().__init__(nbd)
		self.name = 'Lemniscate/Gauss'
		self.link += 'Lemniscate_constant'
	
	def M(self, a, b):
		while (1):
			a, b = a+b >> 1, isqrt(a*b)
			if abs(a-b) < 10:return a+b >> 1
	
	@cache
	def G1(self):
		un = self.un
		unun = un*un
		print('calcule racine de 2')
		debut = time.process_time()
		sqrt2 = isqrt(unun << 1)
		print('in', time.process_time()-debut)
		print('calcule de M(1, racine de 2)')
		debut = time.process_time()
		M1sqrt2 = self.M(un, sqrt2)
		print('in', time.process_time()-debut)
		return unun//M1sqrt2
	
	@cache
	def lemni1(self):
		un = self.un
		unun = un*un
		print('calcule racine de 2')
		debut = time.process_time()
		sqrt2 = isqrt(unun << 1)
		print('in', time.process_time()-debut)
		print('calcule de M(1, racine de 2)')
		debut = time.process_time()
		M1sqrt2 = self.M(un, sqrt2)
		print('in', time.process_time()-debut)
		print('calcule de pi')
		_pi = Pi(nbd)._1()[0]
		print('in', time.process_time()-debut)
		return _pi*un//M1sqrt2
