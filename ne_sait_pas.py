from constantes import *

class Somos(Constante):
	def __init__(self, nbd):
		super().__init__(nbd)
		self.name = 'Somos'
		self.link += ''
	
	@cache
	def _1(self):
		un = self.un
		pr = 1
		prav = 0
		k = 1
		while pr != prav:
			prav = pr
			s = k*un
			for i in range(k):
				s = isqrt(s*un)
			pr = pr*s//un
		return pr

class Heath(Constante):
	def __init__(self, nbd):
		super().__init__(nbd)
		self.name = 'Heath-Brown–Moroz constant'
		self.link += ''
	
	@cache
	def _1(self):
		s = 0
		sav = 1
		p = 0
		while s != sav:
			sav, p = s, nextprime(p)
			s += (un-un//p)**7*(un+(7*p+1)*un//p*p)//un**7
		return s

class RamanujanSoldner(Constante):
	def __init__(self, nbd):
		super().__init__(nbd)
		self.name = 'Ramanujan-Soldner'
	
	def _1(self):
		un, nbd = self.un, self.nbd
		x = self.un*2
		login = LnInte(nbd)
		c = 0
		while (1):
			xav = x
			x -= login._1(x)*self.iln(x, un)//un
			if abs(x-xav) < 10:return x
			c += 1
			print(c, nbd-len(str(abs(x-xav))))
			
class Bruns(Constante):
	def __init__(self, nbd):
		super().__init__(nbd)
		self.name = 'Bruns'
	
	def _1(self, n):
		un, nbd = self.un, self.nbd
		if n == 2:
			f1 = lambda n:isprime(n) and isprime(n+2)
			f2 = lambda n:un//n+un//(n+2)
		elif n==4:
			f1 = lambda n:isprime(n) and isprime(n+2) and isprime(n+6) and isprime(n+ 8)
			f2 = lambda n:un//n+un//(n+2)+un//(n+6)+un//(n+8)
		
		i = 5
		if n == 2:s = un//3+un//5
		else:s = 0
		try:
			while(1):
				if f1(i):
					sav = s
					s += f2(i)
					if s == sav:return s
				i += 2
		except:
			pass
		return s

class Komornik_Loreti(Constante):
	def __init__(self, nbd):
		super().__init__(nbd)
		self.name = 'λ'
	
	def _1(self):
		nbd, un = self.nbd, self.un
		nbs = 100
		a, b = [un]*nbs, [un]*nbs
		t = 2
		while (1):
			a, b = [(-1)**randint(0, 1)*a[i]+\
				(-1)**randint(0, 1)*b[i] for i in range(len(a))], a[:]
			if t%10000 < 2:
				x = 0
				for i in range(len(a)):
					c = abs(a[i])
					if c != 0:
						x += self.expo(self.iln(c, un)//t, un)
				print(x//nbs)
			t += 1

class Meissel(Constante):
	def __init__(self, nbd):
		super().__init__(nbd)
		self.name = 'Meissel-Mertens'
		self.link += 'Meissel%E2%80%93Mertens_constant'
	
	def _1(self):
		gam = mpz(str(sympy.EulerGamma.n(self.nbd*2)).replace('.', '')[1:])
		s = 0
		p = 2
		nbd, un = self.nbd*2, self.un**2
		try:
		#if True:
			while(1):
				sav = s
				s += self.iln(un-un//p, un)+un//p
				if abs(s-sav) < 100:return (s+gam)//self.un
				p = nextprime(p)
		except:return (s+sav >> 1)+gam

class Khinchin(Constante):
	def __init__(self, nbd):
		super().__init__(nbd)
		self.name = 'Khinchin'
	
	def _1(self):
		p = un = self.un
		pav = 0
		n = 2
		nbd = self.nbd
		ln2 = Ln2(nbd)._1()
		try:
			while p != pav:
				pav = p
				pui = un+un//(n*(n+2))
				pui = self.iln(pui, un)
				p2 = self.iln(n*un, un)*un//ln2
				pui = self.expo(pui*p2//un, un)
				p = pui*p//un
				n += 1
			return p
		except:
			return p, n
	
	@cache
	def _2(self):
		c = int(log2(self.nbd))+2
		un_p = 10**c
		un, nbd = self.un*un_p, self.nbd+c
		ln2 = Ln2(nbd)._1()
		s = 0
		sav = 1
		n = 1
		n2 = 2
		t = un
		zeta = Zeta(nbd)
		while(1):
			sav = s
			s += mpz(zeta._1(n2)-un)*t//(n*un)
			t -= un//n2-un//(n2+1)
			n2 += 2
			n += 1
			if abs(s-sav)<un_p//10:return self.expo((s+sav >> 1)*un//ln2, un)//un_p, n//2
			if nbd <= 300:
				print(str(self.expo((s+sav>>1)*un//ln2, un)))
	
	def dilog(self, a, b):
		if a == b:
			return Pi(self.nbd)._1()[0]**2//(6*self.un)
		s = 0
		k = 1
		ak, bk = a, b
		while(1):
			sav = s
			s += ak*self.un//(k*k*bk)
			if s == sav:return s
			ak *= a
			bk *= b
			k += 1
	
	def _3(self):
		nbd, un = self.nbd, self.un
		ln2 = Ln2(nbd)._1()
		s = 0
		k = 2
		o = 1
		while (1):
			sav = s
			s += o*self.dilog(4, (k*k))
			if s == sav:
				return self.expo((s//2+self.dilog(-1, 2))*un//ln2+ln2, un)
			k += 1
			o = -o

class KPrime(Constante):
	def __init__(self, nbd):
		super().__init__(nbd)
		self.name = 'Kepler-Bouwkamp'
	
	def _1(self):
		pass


class Levy1(Constante):
	def __init__(self, nbd):
		super().__init__(nbd)
		self.name = 'beta'
	
	@cache
	def _1(self):
		return Pi(self.nbd)._1()[0]**2//(Ln2(self.nbd)._1()*12)

class Levy2(Constante):
	def __init__(self, nbd):
		super().__init__(nbd)
		self.name = 'Exp(beta)'
	
	@cache
	def _1(self):
		return self.expo(Levy1(nbd)._1(), un)

class Catalan(Constante):#https://en.wikipedia.org/wiki/Catalan%27s_constant
	def __init__(self, nbd):
		super().__init__(nbd)
		self.name = 'Catalan'
		self.link += 'Catalan%27s_constant'
		
	def _1(self):
		s0 = s1 = 0
		sav0 = sav1 = un = self.un
		n = 0
		while s0 != sav0 or s1 != sav1:
			sav0, sav1 = s0, s1
			n8 = 8*n
			n8p2, n8p6 = (n8+2)**2, (n8+6)**2
			s0 += (-un//(n8p2<<1)+\
				un//((n8p2+(n8+2<<1)+1)<<2)-\
				un//((n8p6-(n8+5<<1)+1)<<3)+\
				un//( n8p6             <<3)-\
				un//((n8p2+(n8+6<<1)+1)<<4)+\
				un//((n8p2-(n8+1<<1)+1)<<1))
	
	@cache
	def _2(self):
		s = 0
		sav = 1
		k = 1
		nbd = int(self.nbd*log(self.nbd/1.86))
		un1 = mpz(10)**(nbd-self.nbd)
		un = mpz(10)**nbd
		sfac = 13824*un//(720**3)
		o = -un
		while s != sav:
			sav, a = s, (45136*k**4-57184*k**3+21240*k*k-3160*k+165 << 12*k)*o
			a //= (2*k-1)**3*k**3
			s += a*sfac//un
			k += 1
			sfac, o = sfac*(2*k-1)**3*k**3//((6*k-1)**3*(6*k-5)**3*216), -o
		return (-s>>10)//un1

class Gamma(Constante):#https://en.wikipedia.org/wiki/Euler%27s_constant
	def __init__(self, nbd):
		super().__init__(nbd)
		self.name = 'γ'
		self.link += 'Euler%27s_constant'
	
	def _1(self):
		un = self.un
		i = mpz(1)
		s = mpz(0)
		sav = mpz(1)
		try:
			while s != sav:
				sav = s
				un_on_i = un//i
				s += un_on_i-self.iln(un+un_on_i)
				i += 1
		except:pass
		return s, i
	
	def _2(self):
		un = self.un
		s = mpz(0)
		sav = 1
		k = mpz(2)
		try:
			while s != sav:
				sav = s
				t = (k.bit_length()-1)*un
				s += t//k-t//(k+1)
				k += 2
		except:pass
		return s, k
	
	def _3(self):
		getcontext().prec = self.nbd
		p = 1
		pav = 0
		n = D(1)
		try:
			while p!=pav:
				pav = p
				p *= D.exp(-1/n)*(1+1/n+1/(2*n*n))
				n += 1
		except:pass
		pi_ = D(int(Pi(self.nbd)._1()[0]))/D(int(self.un))
		return D.ln(1/pi_*(D.exp(pi_/2)+D.exp(-pi_/2))/p), n

	def _4(self):
		nbd = self.nbd*3//2
		getcontext().prec = nbd
		un = mpz(10)**nbd
		x = D(4)
		xav = 1
		while abs(x-xav) > D(10)**(-nbd+1) :
			x, xav = (x+2)/D.ln(x), x
		beta = x
		for n in range(int(nbd*0.77), int(nbd*0.77)+2):
			puin = c = mpz(n)*un
			s = -self.iln(c, un)
			fac = mpz(1)
			s2 = mpz(0)
			for k in range(1, int(beta*int(n))+1):
				s += puin//(k*fac)
				fac *= k+1
				puin *= -n
			puin = mpz(1)
			fac = un
			for k in range(n-1):
				s2 += fac//puin
				puin *= -n
				fac *= k+1
			s -= s2*self.expo(mpz(-n)*un, un)//(mpz(n)*un)
			print(str(s)[:self.nbd+2], n)
		return int(str(s)[:self.nbd+2])
	
	@cache
	def _5(self):
		nbd = int(self.nbd)
		un = mpz(10)**nbd
		print('calcul beta ...', end='')
		debut = time.process_time()
		x = 5
		xav = 1
		while x != xav:
			x, xav = (x+3)/log(x), x
		beta = x
		print('in', time.process_time()-debut)
		debut = time.process_time()
		n = int(log(10)/4*nbd)
		sC = sB = sA = mpz(0)
		puin = un
		fac = mpz(1)
		Hk = mpz(0)
		n2 = n*n
		for k in range(int(beta*n)+1):
			sB += puin
			sA += puin*Hk//un
			Hk += un//(k+1)
			puin = puin*n2//(k+1)**2
			if k%(n//5) == 0:
				print('boucle 1:', round(float(k/(n*beta)*100), 2), '% in', round(time.process_time()-debut, 2))
		print('boucle 1 is finish in', time.process_time()-debut)
		debut = time.process_time() 
		np12, fac = (n+1)*(n+1), un
		for k in range(2*n+1):
			sC += fac
			fac = fac*((k<<1)+1)**3//((k+1)*np12 << 5)
			if k%(n//10) == 0:
				print('boucle 2:', round(float(k/(n*2)*100), 2), '% in', round(time.process_time()-debut, 2))
		sC = sC//(n << 2)
		print('boucle 2 is finish in', time.process_time()-debut)
		print('calcul logarithm...')
		debut = time.process_time()
		#c = theta(n, nbd+10)//10**10
		c = self.iln(n*un, un)
		#print(len(str(c-c2)))
		print('in', time.process_time()-debut)
		return (sA*un//sB-sC*un*un//(sB**2)-c)//10**(nbd-self.nbd)
	
	@cache
	def _6(self):
		nbd = int(self.nbd*1004/1000)
		un = mpz(10)**nbd
		x = 1
		xav = 0
		print('calcule du nombre n...', end='')
		debut = time.process_time()
		while abs(x-xav) > 1e-10:
			x, xav = (nbd*log(10)+2**x*(x*log(2)-1))/((2**x+1)*log(2)), x
		n = int(x)+1
		print('in', time.process_time()-debut, '( n =', n, ')')
		s = 0
		t = 0
		dpuin = 1 << n
		dpuimn = un
		print('debut de la boucle')
		debut = time.process_time()
		m = 0
		sav = 1
		while s != sav:
			sav = s
			t += un//(m+1)
			s += dpuimn*t
			if m%10000 == 0:
				print(m, 'in', time.process_time()-debut)
			m += 1
			dpuimn = (dpuimn<<n)//(m+1)
		print('...in', time.process_time()-debut, m)
		print('calcule exp(2**n)...')
		debut = time.process_time()
		puissance = self.expo(dpuin*un, un)
		print('in', time.process_time()-debut)
		s = s*dpuin//puissance
		print('calcule ln(2)...')
		debut = time.process_time()
		ln2 = self.iln2(un << 1, un)
		print('in', time.process_time()-debut)
		s -= ln2*n
		return s//mpz(10)**(nbd-self.nbd)

class Glaisher(Constante):#https://en.wikipedia.org/wiki/Glaisher%E2%80%93Kinkelin_constant
	def __init__(self, nbd):
		super().__init__(nbd)
		self.name = 'Glaisher-Kinkelin'
		self.link += 'Glaisher%E2%80%93Kinkelin_constant'
	
	def icbrt(self, n):
		return mpz(int(cbrt(n)))
	
	@cache
	def _1(self):
		'''
		calcul de la constante par une limite :
		lim(H(n)/(n**(n²/2+n/2+1/12)*exp(-n²/4)))
		  n→oo
		marche bof
		'''
		nbd = self.nbd
		hyperfac = 1
		un = self.un
		t = 1
		tav = 2000
		n = 2
		ppui = mpz(10)**(nbd//2)
		try:
			while (1):
				tav = t
				hyperfac *= n**n
				t1 = n**(6*n**2)
				t2 = n**(6*n)
				t4 = self.expo(n**2*un >> 2, un)
				t = hyperfac*t4*un*un//(self.icbrt(isqrt(isqrt(t1*t2*n*un*un)*un)*un**2)*(un+un//(720*n*n)-1433*un//(7257600*n**4)))
				if abs(tav-t) < ppui:break
				print(len(str(t-tav)), n)
				n += 1
		except:
			pass
		return t+tav >> 1
		
class Gam2(Constante):#formule tiré de l'exponnentielle intégrale grâce à 2 formules différentes donnant le même résultat avec seulement gamma:source:https://en.wikipedia.org/wiki/Exponential_integral formule non exacte.
	def __init__(self, nbd):
		super().__init__(nbd)
		self.name = 'γ'
		self.link += 'Exponential_integral'
	
	def newton(self, f, f_p, x, prec):
		xav = x+prec*2
		while (1):
			x, xav = x-f(x)//f_p(x), xav
			if abs(x - xav) > prec:
				return x+xav >> 1
	
	def nc(self, a, b):
		if a >= 0 and b == 0:
			if a&1 == 0:
				return a+1, b
			else:
				return a-1, b+1
		if b >= 0 and a == 0:
			if b&1:
				return 0, b+1
			else:
				return 1, b-1
		if a+b &1:			    return a-1, b+1
		else:				    return a+1, b-1
	
	@cache
	def _seul(self):
		un, nbd = self.un, self.nbd
		c1 = E1(nbd)._1()
		x = Gamma(nbd)._5()
		expx = self.expo(x, un)
		mini = un//10**2
		a1, b1 = 120245, 7171
		a = self.expo(un*un//(expx-un)+x, un)
		#*(log(
		b = expx-un
		#*
		c = 2179872*expx**6//un**5-\
		    3149496*expx**5//un**4+\
		    1519092*expx**4//un**3-\
		    1013166*expx**3//un**2+\
		    705545 *expx**2//un   -\
		    a1 *expx-b1*un
		#+
		z = 201*(2*expx-un)**2
		d = b*(expx+un)*(z<<2)//un**3
		#*
		e = isqrt(2*(expx-un)*expx*un//(2*expx-un))
		#*
		f = 522*expx**2//un-\
		    261*expx+10*un
		#tdiv//
		g = ((isqrt(un**2 << 3)*b*f+z*e)//un**2)**2//un
		#enddiv+endlog
		#-x)//
		h = a+b
		y = (b*c//un+d*e*f//un**2)*un//g
		xy = a*(self.iln(y,  un)-x)//h+x+c1
		print(xy)
		print('', un)
		if xy > abs(mini):
			print(xy, a1, b1)
			mini = abs(xy)
		a1, b1 = self.nc(a1, b1)
		if a1 == 0:print(a1, b1)
class E1(Constante):
	def __init__(self, nbd):
		self.nbd=nbd
		self.un = mpz(10)**nbd
	
	@cache
	def _1(self):
		s = 0
		sav = un = self.un
		fac = -1
		k = 1
		while s != sav:
			sav = s
			s += un//(fac*k)
			k += 1
			fac *= -k
		return s

