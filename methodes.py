def TypeRamanujan(un, debutfac, transfo, poly, kdeb=0):
	fac = debutfac
	s = 0
	sav = 1
	k = kdeb
	fac = debutfac
	while s != sav:
		sav = s
		s += fac*poly(k)//un
		fac = fac*tranfo(k)//un
	return s

def TypeLimite(flim, prec):
	k = 2
	t = flim(1)
	tav = 0
	while (1):
		tav = t
		t = flim(k)
		if abs(t-tav) < prec:return t
