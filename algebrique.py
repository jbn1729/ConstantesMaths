from constantes import *

class Glaisher(Constante):#https://en.wikipedia.org/wiki/Glaisher%E2%80%93Kinkelin_constant
	def __init__(self, nbd):
		super().__init__(nbd)
		self.name = 'Glaisher-Kinkelin'
		self.link += 'Glaisher%E2%80%93Kinkelin_constant'
	
	def icbrt(self, n):
		return mpz(int(cbrt(n)))
	
	@cache
	def _1(self):
		'''
		calcul de la constante par une limite :
		lim(H(n)/(n**(n²/2+n/2+1/12)*exp(-n²/4)))
		  n→oo
		marche bof
		'''
		nbd = self.nbd
		hyperfac = 1
		un = self.un
		t = 1
		tav = 2000
		n = 2
		ppui = mpz(10)**(nbd//2)
		try:
			while (1):
				tav = t
				hyperfac *= n**n
				t1 = n**(6*n**2)
				t2 = n**(6*n)
				t4 = self.expo(n**2*un >> 2, un)
				t = hyperfac*t4*un*un//(self.icbrt(isqrt(isqrt(t1*t2*n*un*un)*un)*un**2)*(un+un//(720*n*n)-1433*un//(7257600*n**4)))
				if abs(tav-t) < ppui:break
				print(len(str(t-tav)), n)
				n += 1
		except:
			pass
		return t+tav >> 1

class GoldenRatio(Constante):
	def __init__(self, nbd):
		super().__init__(nbd)
		self.name = 'Golden Ratio'
		self.link += ''
		
	@cache
	def _1(self):
		return isqrt(self.un*self.un*5)+1 >> 1
		
class Conway(Constante):
	def __init__(self, nbd):
		super().__init__(nbd)
		self.name = 'λ'
	
	def _1(self):#c'est polinomiale
		un, nbd = self.un, self.nbd
		f = lambda x:(x**71 - x**69 - 2*x**68 - x**67 + 2*x**66 + 2*x**65 + x**64 - x**63 - x**62   - x**61  - x**60   - x**59  +2*x**58  + 5*x**57 + 3*x**56 - 2*x**55 - 10*x**54 - 3*x**53 - 2*x**52 + 6*x**51+ 6*x**50 + x**49  + 9*x**48 - 3*x**47 - 7*x**46 - 8*x**45 - 8*x**44  + 10*x**43  + 6*x**42+ 8*x**41 - 5*x**40- 12*x**39+ 7*x**38 - 7*x**37 + 7*x**36 + x**35    - 3*x**34 + 10*x**33+ x**32  - 6*x**31 - 2*x**30- 10*x**29- 3*x**28 + 2*x**27 + 9*x**26 - 3*x**25  + 14*x**24  - 8*x**23- 7*x**21 + 9*x**20+ 3*x**19 - 4*x**18 - 10*x**17- 7*x**16 + 12*x**15 + 7*x**14   + 2*x**13- 12*x**12- 4*x**11- 2*x**10 + 5*x**9  + x**7    - 7*x**6  + 7*x**5   - 4*x**4 + 12*x**3   - 6*x*x  + 3*x - 6)
		f_p = lambda x:(74*x**70-69*x**68-136*x**67-67*x**66+132*x**65+130*x**64+64*x**63-63*x**62-62*x**61-61*x*60- 60*x**59-59*x**58+116*x**57+285*x**56+168*x**55-110*x**54-540*x**53-159*x**52-104*x**51+ 306*x**50+300*x**49+49*x**48+432*x**47-141*x**46-322*x**45-360*x**44-352*x**43+430*x**42+ 252*x**41+328*x**40-200*x**39-468*x**38+266*x**37-259*x**36+252*x**35+35*x**34-102*x**33+ 330*x**32+32*x**31-186*x**30-60*x**29-290*x**28-84*x**27+54*x**26+234*x**25-75*x**24+336*x**23-184*x**22- 147*x**20+180*x**19+57*x**18-72*x**17-170*x**16-112*x**15+180*x**14+98*x**13+26*x**12-144*x**11- 44*x**10-20*x**9+45*x**8+7*x**6-42*x**5+35*x**4-16*x**3+36*x**2-12*x+3)
		x, xav = Integer(13*un//10, un), Integer(un, un)
		while x!=xav:
			xav, x = x, x-f(x)//f_p(x)
		return x

