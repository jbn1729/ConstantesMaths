from constantes import *

class Fibo(Constante):
	def __init__(self, nbd):
		super().__init__(nbd)
		self.name = 'sum of inverse fibonacci'
		self.link += ''
	
	@cache
	def _1(self):
		a, b = 1, 1
		un = self.un
		s = 2
		sav = 0
		while s != sav:
			a, b, sav = a+b, a, s
			s += un//a
		return s

class Apery(Constante):
	def __init__(self, nbd):
		super().__init__(nbd)
		self.name = 'Apery'
	
	def _1(self):
		k = 0
		un, nbd = self.un*10**int(self.nbd/10), int(self.nbd+self.nbd/10)
		fac = un//(2*6**3)
		s = 0
		spoly = 12463
		while (1):
			sav = s
			s += fac*(126392*k**5+412708*k**4+531578*k**3+spoly)
			if s == sav:return s//(mpz(10)**(nbd-self.nbd)*24)
			k += 1
			spoly += 104000+336367*(2*k-1)
			fac = (-fac*k**5*(2*k-1)**3)//(24*(3*k+1)*(3*k+2)*(4*k+1)**3*(4*k+3)**3)


class Van_der_Pauw(Constante):
	def __init__(self, nbd):
		super().__init__(nbd)
		self.name = 'Pi_on_ln2'
	
	@cache
	def _1(self):
		return Pi(self.nbd)._1()[0]*un//(Ln2(nbd)._1())


