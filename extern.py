def bsp_acot(q, a, b, hyperbolic):
	if b - a == 1:
		a1 = mpz((a<<1) + 3)
		if hyperbolic or a&1:
			return mpz(1), a1 * q**2, a1
		else:
			return -mpz(1), a1 * q**2, a1
	m = (a+b) >> 1
	p1, q1, r1 = bsp_acot(q, a, m, hyperbolic)
	p2, q2, r2 = bsp_acot(q, m, b, hyperbolic)
	return q2*p1 + r1*p2, q1*q2, r1*r2

def iM(a, b):
	while abs(a-b) > 1:
		a, b = (a+b)//2, isqrt(a*b)
	return a

def M(a, b):
	while abs(a-b) > 1e-15:
		a, b = (a+b)/2, sqrt(a*b)
	return a

def remet(n, un, pui):
	return mpz(n)//un**mpz(pui-1)

def theta(n, prec=20):
	un = mpz(10)**prec
	s0 = mpz(0)
	s1 = mpz(0)
	s1av = 1
	s0av = 1
	t = isqrt(isqrt(un**4//n))#{**(1/4)
	k=0
	s0 += un+(un//n)
	s1 += t << 1
	k = 1
	for i in range(10):
		s1av, s0av = s1, s0
		s0 += remet((un//n)**k**2, un, k**2)+remet((un//n)**(k**2+2*k+1), un, k**2+2*k+1)
		s1 += remet((un//n)**(k**2+k)*t << 1, un, (k**2+k)+1)
		#s1 += (un/n)**(k**2+k)*t
		k += 1
	si0, si1 = s0, s1
	integert = iM(s1**2//un, s0**2//un)
	_pi = Pi(prec)._1()
	return mpz(_pi[0]*un)//integert

def acot_fixed(a, prec, hyperbolic):
	"""
	Compute acot(a) or acoth(a) for an integer a with binary splitting; see
	http://numbers.computation.free.fr/Constants/Algorithms/splitting.html
	"""
	N = int(0.35 * prec/math.log(a) + 20)
	p, q, r = bsp_acot(a, 0,N, hyperbolic)
	return ((p+q)<<prec)//(q*a)		

def machin(coefs, prec, hyperbolic=False):
	"""
	Evaluate a Machin-like formula, i.e., a linear combination of
	acot(n) or acoth(n) for specific integer values of n, using fixed-
	point arithmetic. The input should be a list [(c, n), ...], giving
	c*acot[h](n) + ...
	"""
	extraprec = 10
	s = mpz(0)
	for a, b in coefs:
		s += mpz(a) * acot_fixed(mpz(b), prec+extraprec, hyperbolic)
	return (s >> extraprec)

# Logarithms of integers are needed for various computations involving
# logarithms, powers, radix conversion, etc

def ln2_fixed(prec):
	"""
	Computes ln(2). This is done with a hyperbolic Machin-type formula,
	with binary splitting at high precision.
	"""
	return machin([(18, 26), (-2, 4801), (8, 8749)], prec, True)

"""
Euler's constant (gamma) is computed using the Brent-McMillan formula,
gamma ~= I(n)/J(n) - log(n), where

   I(n) = sum_{k=0,1,2,...} (n**k / k!)**2 * H(k)
   J(n) = sum_{k=0,1,2,...} (n**k / k!)**2
   H(k) = 1 + 1/2 + 1/3 + ... + 1/k

The error is bounded by O(exp(-4n)). Choosing n to be a power
of two, 2**p, the logarithm becomes particularly easy to calculate.[1]

We use the formulation of Algorithm 3.9 in [2] to make the summation
more efficient.

Reference:
[1] Xavier Gourdon & Pascal Sebah, The Euler constant: gamma
http://numbers.computation.free.fr/Constants/Gamma/gamma.pdf

[2] Jonathan Borwein & David Bailey, Mathematics by Experiment,
A K Peters, 2003
"""

def euler_fixed(prec):
	extra = 30
	prec += extra
	# choose p such that exp(-4*(2**p)) < 2**-n
	p = int(math.log((prec/4) * math.log(2), 2)) + 1
	n = 1 << p
	n2 = n*n
	A = U = -p*ln2_fixed(prec)
	B = V = mpz(1) << prec
	k = 1
	while 1:
		B = B*n2//k**2
		A = (A*n2//k + B)//k
		U += A
		V += B
		if max(abs(A), abs(B)) < 100:
			break
		k += 1
	return (U<<(prec-extra))//V
