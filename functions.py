from constantes import *
from transcendant import Pi, E, Gamma

###########################fonction de Riemmann#################################
class R(Constante):# no gut page ( one in france but no gut)
	def __init__(self, nbd, x):
		super().__init__(nbd)
		self.x = x
		self.name = f'R({x})'
	
	@cache
	def _1(self):
		s = 1
		getcontext().prec = 100
		x = self.x
		pui = lnx = D.ln(D(x))
		k = 1
		sav = 2
		fac = 1
		while s != sav:
			sav = s
			zeta_ = D(str(sympy.zeta(k+1).n(100)))
			s += D(pui)/(zeta_*fac*k)
			pui *= lnx
			k += 1
			fac *= k
		return int(s)

#################fonction de comptage des nombres premiers######################
@cache
def prime_k(n):
	return prime(n)

class Comptage(Constante):#https://en.wikipedia.org/wiki/Prime-counting_function
	def __init__(self):
		super().__init__(0)
		self.name = f'pi({})'
		self.comptages_to_100 = tuple(sum(1 for i in range(2, k+1) if isprime(i)) for k in range(101))
	def phi(self, a, b):
		if not isprime(b):
			return self.phi(a, b-1)
		else:
			return self.phi(a, b-1)-self.phi(a/b, b-1)+self.phi(b-1, b-1)
	
	def fonc(self, m, b):
		return self.fonc(m, b-1)-self.fonc(m/prime_k(b), b-1) if b != 0 else int(m)

	@cache
	def result1(self, m):
		if m <= 100:
			return self.comptages_to_100[m]
		else:
			c = int(m**(1/3))+1
			d = int(m**(1/2))+1
			n = self.result1(c)
			mu = self.result1(d)-n
			s = 0
			for k in range(1, mu+1):
				s += self.result1(m//prime_k(n+k))
			return int(self.fonc(m, n)+n*(mu+1)+(mu**2-mu)//2-1-s)
	
	@cache
	def eval(self, n):

		try:
			n = int(n)
		except TypeError:
			if n.is_real == False or n is S.NaN:
				raise ValueError("n must be real")
			return

		if n < 2:
			return 0
		if n < 101:
			return self.comptages_to_100[n]
		lim = isqrt(n)
		lim -= 1
		lim = max(lim, 0)
		while lim * lim <= n:
			lim += 1
		lim -= 1
		arr1 = [0] * (lim + 1)
		arr2 = [0] * (lim + 1)
		for i in range(1, lim + 1):
			arr1[i], arr2[i] = i - 1, n // i - 1
		for i in range(2, lim + 1):
			# Presently, arr1[k]=phi(k,i - 1),
			# arr2[k] = phi(n // k,i - 1)
			if arr1[i] == arr1[i - 1]:
				continue
			p = arr1[i - 1]
			for j in range(1, min(n // (i * i), lim) + 1):
				st = i * j
				if st <= lim:
					arr2[j] -= arr2[st] - p
				else:
					arr2[j] -= arr1[n // st] - p
			lim2 = min(lim, i * i - 1)
			for j in range(lim, lim2, -1):
				arr1[j] -= arr1[j // i] - p
		return (arr2[1])
if __name__ == '__main__' and False:
	ref = [4, 25, 168, 1229, 9592, 78498, 664579, 5761455, 50847534, 455052511, 4118054813, 37607912018, 346065536839, 3204941750802, 29844570422669, 279238341033925, 2623557157654233, 24739954287740860, 234057667276344607, 2220819602560918840, 21127269486018731928, 201467286689315906290, 1925320391606803968923, 18435599767349200867866, 176846309399143769411680, 1699246750872437141327603, 16352460426841680446427399, 157589269275973410412739598, 1520698109714272166094258063]
	t = Comptage()
	for x, refe in zip(range(1, 30), ref):
		debut = time.process_time()
		n = t.eval(10**x)
		print(round(time.process_time()-debut, 2), n, '\n    ', refe, int(n-refe))
####################################zeta########################################
class Zeta(Constante):#https://en.wikipedia.org/wiki/Riemann_zeta_function
	def __init__(self, nbd):
		super().__init__(nbd)
		self.name = 'zeta'
		self.link += 'Riemann_zeta_function'
	
	@cache
	def _1(self, s):
		if s&1:
			so = 0
			un = self.un
			sav = 10**10
			n = 1
			som = n
			n2 = 2
			sp2 = s+2
			try:
				while so != sav:
					sav = so
					t1 = som*un
					t2 = (n2+3+s)*un//(n+1)**sp2
					t3 = (n2-1-s)*un//n**sp2
					so += t1*(t2-t3)//un
					n += 1
					n2 += 2
					som += n
			except:pass
			return int(so)
		else:
			nbd, un = self.nbd, self.un
			ber = abs(Bernoulli(nbd)._1(s))
			deno = 2*factorial(s)
			pipui = ((Pi(nbd)._1()[0]<<1)**s)//un**(s-1)
			return ber*pipui//(un*deno)
##################################bernoulli#####################################
class Bernoulli(Constante):
	def __init__(self, nbd):
		super().__init__(nbd)
		self.name = 'Bernoulli'
	
	def parmi(self, n, k):
		return factorial(n)//(factorial(k)*factorial(n-k))
	
	@cache
	def _1(self, n):
		s = 0
		un = self.un
		for k in range(n+1):
			serie = 0
			o = 1
			for v in range(k+1):
				serie += o*(v+1)**n*self.parmi(k, v)
				o = -o
			s += Fraction(serie, k+1)
		return s.numerator*un//s.denominator
#################################Logarithme intégrale###########################
class LnInte(Constante):
	def __init__(self, nbd):
		super().__init__(nbd)
		self.name = 'Ln intégrale'
	
	def _1(self, n):
		if isinstance(n, int):n *= self.un
		s = 0
		un, nbd = self.un, self.nbd
		gam = Gamma(self.nbd)._5()
		pui = lnn = self.iln(n, un)
		s = self.iln(abs(pui), un)+gam
		sav = 1
		fac = 1
		k = 1
		while (1):
			sav = s
			s += pui//(fac*k)
			if s == sav:
				return s
			k += 1
			pui = pui*lnn//un
			fac *= k
	
	def _2(self, n):
		if isinstance(n, int):n *= self.un
		s = 0
		un, nbd = self.un, self.nbd
		gam = Gamma(self.nbd)._5()
		pui = lnn = self.iln(n, un)
		lnn = -lnn
		sp = self.iln(abs(pui), un)+gam
		s = 0
		s2 = un
		fac = 1
		k = 1
		try:
			while(1):
				sav = s
				s += pui//(fac << (k-1))*s2//un
				if s == sav:
					return isqrt(n*un)*s//un+sp
				k += 1
				if k&1:
					s2 += un//(k)
				fac *= k
				pui = pui*lnn//un
		except:return isqrt(n*un)*s//un+sp
