#################################importation####################################
from functions import Zeta, Bernoulli, Comptage, R, LnInte
from gmpy2 import mpz, isqrt, log, exp, cbrt, inf, mpfr
from math import sqrt, log2, pi, log10, atan, factorial
from decimal import Decimal as D, getcontext
from sympy import prime, isprime, nextprime
from fractions import Fraction
from functools import cache
import time, sympy, math
###################################Integer######################################
class Integer:
	def __init__(self, n, un):
		self.n, self.un = n, un
	
	def __add__(self, other):
		if isinstance(other, (int, mpz)):
			other = Integer(other*self.un, self.un)
		if isinstance(other, Integer):
			un, n = self.un, self.n
			if self.un > other.un:
				other = Integer(other.n*self.un//other.un, self.un)
			elif self.un < other.un:
				un, n = self.un, self.n
				n *= other.un//self.un
				un = other.un
			return Integer(n+other.n, self.un)
	
	def __sub__(self, other):
		if isinstance(other, (int, mpz)):
			other = Integer(other*self.un, self.un)
		if isinstance(other, Integer):
			un, n = self.un, self.n
			if self.un > other.un:
				other = Integer(other.n*self.un//other.un, self.un)
			elif self.un < other.un:
				un, n = self.un, self.n
				n *= other.un//self.un
				un = other.un
			return Integer(n-other.n, self.un)
			
	def __mul__(self, other):
		if isinstance(other, (int, mpz)):
			return Integer(self.n*other, self.un)
		if isinstance(other, Integer):
			un, n = self.un, self.n
			if self.un > other.un:
				other = Integer(other.n*self.un//other.un, self.un)
			elif self.un < other.un:
				un, n = self.un, self.n
				n *= other.un//self.un
				un = other.un
			return Integer(n*other.n//un, un)
	
	def __rmul__(self, other):
		return self.__mul__(other)
	
	def __floordiv__(self, other):
		if isinstance(other, (int, mpz)):
			other = Integer(other*un, un)
		if isinstance(other, Integer):
			un, n = self.un, self.n
			if self.un > other.un:
				other = Integer(other.n*self.un//other.un, self.un)
			elif self.un < other.un:
				un, n = self.un, self.n
				n *= other.un//self.un
				un = other.un
			return Integer(n*un//other.n, un)
	
	def __pow__(self, other):
		return Integer(self.n**other//self.un**(other-1), self.un)
	
	def __repr__(self):
		return str(self.n)
	
	def __eq__(self, other):
		return self.n == other.n and other.un == self.un
		
#################################présentation###################################
class Constante:
	def __init__(self, nbd):
		# Initialisation commune à toutes les constantes
		self.name = "Default"
		self.link = 'https://en.wikipedia.org/wiki/'
		self.nbd = nbd
		self.un = mpz(10)**nbd

	def expo(self, x, un):
		s = x+un
		i = un<<1
		pui = x*x//i
		sav = 0
		while (s != sav):
			sav = s
			s += pui
			i += un
			pui = pui*x//i
		return s
	
	def iln(self, x, un):
		y = mpz(int(log(x/un)*un))
		#y = theta(x//un, len(str(un)))
		yav = 1
		un2 = 2*un
		n = 1
		debut = time.process_time()
		while abs(y-yav) > 10:
			yav, c = y, self.expo(y, un)
			y += un2*(x-c)//(x+c)
			#print(n, 'ème passage in ', time.process_time()-debut, sep='')
			n += 1
		return y
	
	def __repr__(self):
		# Méthode commune pour afficher le nom de la constante
		return f"{self.name} [{self.nbd}] = {self.calcul()}"

def MM(a, b):
	while (1):
		a, b = (a+b)/2, sqrt(a*b)
		if abs(a-b) < 1e-15:return (a+b)/2

def f(pi_):
	pi_/= 10
	mms2 = MM(1, sqrt(2))
	G = 1/mms2
	A = 1/2*pi_*G
	B = 1/(2*G)
	return A*B*4-pi_

##############################mesure de temps###################################
def temps(f, times):
	s, debut = 0, 0
	for i in range(times):
		debut = time.process_time()
		c = f()
		s += time.process_time()-debut
	return round(s/times, 9)
