import matplotlib as mpl, matplotlib.pyplot as plt, numpy as np
def dessine_hist(data_y, fig=None, ax=None):
	plt.ion()
	if fig is None or ax is None:
		fig, ax = plt.subplots(figsize=(5, 2.7))
	n, bins, patches = ax.hist(data_y, 50, density=1, facecolor='#FF0000', alpha=0.75)
	ax.grid(True)

def dessine_point(data_x, data_y, taille, fig=None, ax=None):
	plt.ion()
	if fig is None or ax is None:
		fig, ax = plt.subplots(figsize=(5, 2.7))
	ax.scatter(data_x, data_y, s=taille, facecolor='#FF0000')
	return fig, ax

def dessine_curve(data_x, data_y, fig=None, ax=None):
	plt.ion()
	if fig is None or ax is None:
		fig, ax = plt.subplots(figsize=(5, 2.7))
	ax.plot(data_x, data_y)
	return fig, ax

def dessine_f(f, a, b, pas=1, ax=None, fig=None):
	return dessine_curve(list(range(a, b+1, pas)), list(map(f, range(a, b+1))), fig, ax)

